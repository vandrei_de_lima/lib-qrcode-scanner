import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { VIDEO_CONFIG } from './qrcode-scanner.consts';
import { Subject, takeUntil, timer } from 'rxjs';

import { QrcodeScannerService } from './qrcode-scanner.service';
import jsQR from 'jsQR';

@Component({
  selector: 'qrcode-scanner',
  template: `
    <div class="video--container">
      <video #videoElement autoplay playsinline></video>
    </div>
    <div class="video--container" style="display: none">
      <canvas id="canvas" #canvas></canvas>
    </div>
  `,
  styles: [
    `
      .video--container {
        padding: 10px;
        video {
          width: 100%;
          border-radius: 6px;
        }
      }
    `,
  ],
  preserveWhitespaces: false,
  encapsulation: ViewEncapsulation.None,
})
export class QrcodeScannerComponent implements AfterViewInit, OnDestroy {
  @Input() config = structuredClone(VIDEO_CONFIG);

  @Output() resultEvent = new EventEmitter<string>();

  @ViewChild('videoElement') video!: ElementRef<HTMLVideoElement>;
  @ViewChild('canvas', { static: true }) canvas!: ElementRef;

  videoStream!: MediaStream;

  private destroy$ = new Subject<void>();

  constructor(private service: QrcodeScannerService) {}

  ngAfterViewInit(): void {
    this.service.changeCamera$.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.changeCamera();
    });

    this.prepareScanner();
  }

  async prepareScanner() {
    const available = await this.checkCamera();
    if (available) this.startScanner();
  }

  changeCamera() {
    let { facingMode } = this.config.video;

    this.config.video.facingMode =
      facingMode === 'enviroment' ? 'user' : 'enviroment';
    this.startScanner();
  }

  async startScanner() {
    this.videoStream = await navigator.mediaDevices.getUserMedia(this.config);
    this.video.nativeElement.srcObject = this.videoStream;

    this.spyCamera();
  }

  spyCamera() {
    if (this.video.nativeElement) {
      const { clientWidth, clientHeight } = this.video.nativeElement;

      this.canvas.nativeElement.width = clientWidth;
      this.canvas.nativeElement.height = clientHeight;

      const canvas = this.canvas.nativeElement.getContext(
        '2d'
      ) as CanvasRenderingContext2D;

      canvas.drawImage(
        this.video.nativeElement,
        0,
        0,
        clientWidth,
        clientHeight
      );

      const inversionAttempts = 'dontInvert';

      const image = canvas.getImageData(0, 0, clientWidth, clientHeight);
      const qrcode = jsQR(image.data, image.width, clientHeight, {
        inversionAttempts,
      });
      if (qrcode) {
        const { data } = qrcode;

        this.resultEvent.emit(data);
        this.service.result$.next(data);

        this.spyDelay(500);
      } else {
        this.spyDelay();
      }
    }
  }

  spyDelay(delay: number = 200) {
    timer(delay)
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.spyCamera();
      });
  }

  async checkCamera() {
    const cameraPermissions = await navigator.permissions.query({
      name: 'camera',
    } as any);

    const isOk = cameraPermissions.state !== 'denied';

    const hasMediaDevice = 'mediaDevices' in navigator;
    const hasUserMedia = 'getUserMedia' in navigator.mediaDevices;

    if (!hasMediaDevice || (!hasUserMedia && isOk)) {
      this.service.errors$.next('not has accesss');
    }

    return cameraPermissions.state !== 'denied';
  }

  ngOnDestroy() {
    this.videoStream.getTracks().forEach((track) => track.stop());
    this.video = null!;

    this.destroy$.next();
    this.destroy$.complete();
  }
}
