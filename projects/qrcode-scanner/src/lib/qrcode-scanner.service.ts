import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class QrcodeScannerService {
  result$ = new BehaviorSubject<string>('')

  errors$ = new Subject<string>()

  changeCamera$  = new Subject<void>()
}
