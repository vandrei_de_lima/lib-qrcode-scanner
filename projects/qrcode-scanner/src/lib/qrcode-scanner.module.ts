import { NgModule } from '@angular/core';
import { QrcodeScannerComponent } from './qrcode-scanner.component';



@NgModule({
  declarations: [
    QrcodeScannerComponent
  ],
  imports: [
  ],
  exports: [
    QrcodeScannerComponent
  ]
})
export class QrcodeScannerModule { }
