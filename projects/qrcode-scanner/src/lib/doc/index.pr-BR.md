---
title: Scaner component
---

Simples scanner para qrcode.

## Quando usar?

Esse scanner pode ser usado quando voce precisar obter informações de algum qrcode, em qualquer aplicação angular, ionic/PWA.

```ts
import { QrcodeScannerModule } from 'qrcode-scanner';
```

## API

```html
  <qrcode-scanner (resultEvent)="result($event)" [config]="yourConfig"></qrcode-scanner>
```

### [qrcode-scanner]

| Propiedade | Descrição | Tipo | Valor padrão | Configuração global |
| -------- | ----------- | ---- | ------- | ------------- |
| `[config]` | Configuração de scanner, ja tem uma padrão mas poder ser customizada | `IVideoConfig` |
| `(resultEvent)` | Esse evento retorna o resultado do qrcode | `string` | `empty` |

`Configuração padrao e exomplo da estrutura do config`

```ts
const VIDEO_CONFIG = {
  video: {
    width: {min: 250, max: 450, ideal: 400},
    height: {min: 250, max: 450, ideal: 400},
    facingMode: 'enviroment'
  }
}
```

### QrcodeScannerService

| Propiedade | Descrição | Tipo | 
| -------- | ----------- | ---- | 
| `result$` | BehaviorSubject que retorna e armazena o valor do qrcode | `BehaviorSubject - string` |
| `errors$` | Subject que retorna o error internos | `Subject - string` | 
| `changeCamera$` |  Subject para acionar a troca de camera | `Subject - void` | 


