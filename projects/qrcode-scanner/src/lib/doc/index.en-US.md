---
title: Scaner component
---

Simple scanner to qrcode.

## When To Use

this scanner is used when you need to get information from qrcode using angular/ionic application. is recommended for pwa also

```ts
import { QrcodeScannerModule } from 'qrcode-scanner';
```

## API

```html
  <qrcode-scanner (resultEvent)="result($event)" [config]="yourConfig"></qrcode-scanner>
```

### [qrcode-scanner]

| Property | Description | Type | Default | Global Config |
| -------- | ----------- | ---- | ------- | ------------- |
| `[config]` | this config hava a default but you can send a custom config | `IVideoConfig` |
| `(resultEvent)` | this is return of scanned qrcode | `string` | `empty` |

`Default config and structure exemple`

```ts
const VIDEO_CONFIG = {
  video: {
    width: {min: 250, max: 450, ideal: 400},
    height: {min: 250, max: 450, ideal: 400},
    facingMode: 'enviroment'
  }
}
```

### QrcodeScannerService

| Property | Description | Type
| -------- | ----------- | ---- | 
| `result$` | BehaviorSubject return and save qrcode data | `BehaviorSubject - string` |
| `errors$` | Subject return internal erros | `Subject - string` | 
| `changeCamera$` |  Subject trigger change camera | `Subject - void` | 
