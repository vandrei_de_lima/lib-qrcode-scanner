# Scanner

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.11.

## Development server

For use install
 - npm i jsqr --save


Simple scanner to qrcode.

## When To Use

this scanner is used when you need to get information from qrcode using angular/ionic application. is recommended for pwa also

this lib use 
```url
https://github.com/cozmo/jsQR
```



## API

```ts
import { QrcodeScannerModule } from 'qrcode-scanner';
```

```html
  <qrcode-scanner (resultEvent)="result($event)" [config]="yourConfig"></qrcode-scanner>
```

### [qrcode-scanner]

| Property | Description | Type | Default | Global Config |
| -------- | ----------- | ---- | ------- | ------------- |
| `[config]` | this config hava a default but you can send a custom config | `IVideoConfig` |
| `(resultEvent)` | this is return of scanned qrcode | `string` | `empty` |

`Default config and structure exemple`

```ts
const VIDEO_CONFIG = {
  video: {
    width: {min: 250, max: 450, ideal: 400},
    height: {min: 250, max: 450, ideal: 400},
    facingMode: 'enviroment'
  }
}
```

### QrcodeScannerService

| Property | Description | Type
| -------- | ----------- | ---- | 
| `result$` | BehaviorSubject return and save qrcode data | `BehaviorSubject - string` |
| `errors$` | Subject return internal erros | `Subject - string` | 
| `changeCamera$` |  Subject trigger change camera | `Subject - void` | 
